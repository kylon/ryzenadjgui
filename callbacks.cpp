#include <FL/fl_ask.H>

#include "utils.h"

extern std::vector<option_t> optsVec;

void dialogOk(Fl_Button *b, void *window) {
    if (Fl::event_button() != FL_LEFT_MOUSE)
        return;

    if (b->value() == 0) // button released
        static_cast<Fl_Window *>(window)->hide();
}

void closeWindow(Fl_Widget *w, void *window) {
    static_cast<Fl_Window *>(window)->hide();
}

void setOptionState(Fl_Check_Button *b, void *data) {
    cbData cdata = *static_cast<cbData *>(data);

    optsVec[cdata.oIdx].active = b->value();
    cdata.changedLst.insert(optsVec[cdata.oIdx].id);
    LOG("enable "<<optsVec[cdata.oIdx].label<<": "<<optsVec[cdata.oIdx].active)

    if (optsVec[cdata.oIdx].active)
        b->parent()->child(cdata.widgetIdx)->show();
}

void setOption(Fl_Hor_Value_Slider *s, void *data) {
   cbData cdata = *static_cast<cbData *>(data);

    switch (optsVec[cdata.oIdx].id) {
        case OPTID::STAPMLIM:
            cdata.sett->stapmLim = s->value();
            break;
        case OPTID::FASTLIM:
            cdata.sett->fastLim = s->value();
            break;
        case OPTID::SLOWLIM:
            cdata.sett->slowLim = s->value();
            break;
        case OPTID::SLOWTIME:
            cdata.sett->slowTime = s->value();
            break;
        case OPTID::STAPMTIME:
            cdata.sett->stapmTime = s->value();
            break;
        case OPTID::TCTLTEMP:
            cdata.sett->tctlTemp = s->value();
            break;
        case OPTID::VRMCURRENT:
            cdata.sett->vrmCurrent = s->value();
            break;
        case OPTID::VRMMAXCURRENT:
            cdata.sett->vrmMaxCurrent = s->value();
            break;
        case OPTID::PSI0CURRENT:
            cdata.sett->psi0Current = s->value();
            break;
        case OPTID::MAXSOCCLOCK:
            cdata.sett->maxSocClkFrequency = s->value();
            break;
        case OPTID::MINSOCCLOCK:
            cdata.sett->minSocClkFrequency = s->value();
            break;
        case OPTID::MAXFCLOCK:
            cdata.sett->maxFclkFrequency = s->value();
            break;
        case OPTID::MINFCLOKC:
            cdata.sett->minFclkFrequency = s->value();
            break;
        case OPTID::MAXGFXCLOCK:
            cdata.sett->maxGfxClk = s->value();
            break;
        case OPTID::MINGFXCLOCK:
            cdata.sett->minGfxClk = s->value();
            break;
        default:
            break;
    }

    cdata.changedLst.insert(optsVec[cdata.oIdx].id);

    if (optsVec[cdata.oIdx].active)
        s->parent()->child(cdata.widgetIdx)->show();
}

void resetSliders(Fl_Widget *w, void *data) {
    cbData3 *cdata = static_cast<cbData3 *>(data);
    std::unique_ptr<cbData> cdata2 = std::make_unique<cbData>(cdata->widgetIdx, cdata->oIdx, cdata->sett, cdata->changedLst);
    bool stateChanged = false;
    int i = 0;

    for (auto const &s : cdata->sliders) {
        while (i < optsVec.size() && !optsVec[i].enabled)
            ++i;

        if (i >= optsVec.size())
            break;

        cdata2->oIdx = i;

        if (s->value() != optsVec[i].defVal) {
            if (optsVec[i].active)
                stateChanged = true;

            s->value(optsVec[i].defVal);
            s->do_callback(s.get(), cdata2.get());
        }

        ++i;
    }

    if (stateChanged)
        w->parent()->child(cdata->widgetIdx)->show();
}

void applySett(Fl_Button *btn, void *data) {
    int ret = fl_choice("Do you want to apply current settings?\n\nThis may harm your pc!",
                "Maybe later...",
                "Do it!", 0);

    if (!ret)
        return;

    cbData3 cdata = *static_cast<cbData3 *>(data);
    ryzen_access ry = init_ryzenadj();
    std::string notApplied;
    int i = 0;

#ifndef DBG_UNSUP
    if (ry == nullptr) {
        fl_alert("Failed to initialize RyzenAdj.\n\nUnsupported CPU?");
        return;
    }
#endif

    for (auto const &s : cdata.sliders) {
        while (i < optsVec.size() && !optsVec[i].enabled)
            ++i;

        if (i >= optsVec.size())
            break;

        if (!optsVec[i].active || cdata.changedLst.find(optsVec[i].id) == cdata.changedLst.end()) {
            ++i;
            continue;
        }

        uint32_t v = s->value();

        ret = writeSMU(ry, v, optsVec[i].id);
        if (ret < 0)
            notApplied += optsVec[i].label + "\n";
        else
            cdata.changedLst.erase(optsVec[i].id);

        LOG("WriteSMU "<<optsVec[i].label<<" ret: "<<std::to_string(ret))
        ++i;
    }

    cleanup_ryzenadj(ry);

    if (!notApplied.empty())
        fl_alert("Error: Some settings could not be applied!\n\n%s", notApplied.c_str());
    else
        btn->hide();
}

void saveSett(Fl_Widget *, void *data) {
    cbData2 cdata = *static_cast<cbData2 *>(data);
    std::filesystem::path dir = cdata.settPath;

    dir.remove_filename();
    if (!std::filesystem::exists(dir)) {
        bool ret = std::filesystem::create_directory(dir);

        if (!ret) {
            fl_alert("Error: Cannot create directory.\n\nFailed to save settings!");
            return;
        }
    }

    std::ofstream out {cdata.settPath, std::ios::binary};

    if (!out.is_open()) {
        fl_alert("Error: Cannot create settings file!");
        return;
    }

    out.write(reinterpret_cast<const char *>(cdata.sett), sizeof(settings_t));
}

void setIntervalSett(Fl_Counter *c, void *data) {
    cbData2 cdata = *static_cast<cbData2 *>(data);

    cdata.sett->reapplySecs = c->value();
}

static void applyThreadFn(cbData3 *data) {
    ryzen_access ry = init_ryzenadj();
    int ret = -1;
    int i = 0;

#ifndef DBG_UNSUP
    if (ry == nullptr) {
        LOG("applyThreadFn: ryzenadj init error")
        return;
    }
#endif

    for (auto const &s : data->sliders) {
        while (i < optsVec.size() && !optsVec[i].enabled)
            ++i;

        if (i >= optsVec.size()) {
            break;

        } else if (!optsVec[i].active) {
            ++i;
            continue;
        }

        ret = writeSMU(ry, s->value(), optsVec[i].id);

        LOG("thread: writeSMU ret: " << std::to_string(ret))
        ++i;
    }

    cleanup_ryzenadj(ry);

    if (data->sliders.size())
        data->sliders[0]->parent()->child(data->widgetIdx)->hide();
}

void applyThread(Fl_Check_Button *b, void *data) {
    cbData4 cdata = *static_cast<cbData4 *>(data);

    if (!cdata.thrdRunFlag.load()) {
        cdata.thrdRunFlag.store(true);
        startInterval(applyThreadFn, cdata.cbdata, cdata.thrdRunFlag, cdata.cbdata->sett->reapplySecs);
        LOG("applyThread start [interval: " << cdata.cbdata->sett->reapplySecs << " secs]")

    } else {
        cdata.thrdRunFlag.store(false);
        LOG("applyThread stop")
    }
}