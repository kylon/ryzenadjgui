#pragma once

#include "utils.h"

void help(const std::string &);
RETCODE parseArgs(int, char **, uint &);
RETCODE daemonize();
void startDaemon(const std::unique_ptr<settings_t> &, uint, const std::filesystem::path &);
RETCODE stopDaemon(const std::string &);