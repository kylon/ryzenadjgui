#pragma once

#include <functional>
#include <ryzenadj.h>

#include "common.h"

bool isRoot();
RETCODE getRunningPid(const std::string &, std::string &, bool);
bool isAlreadyRunning(const std::string &);
RETCODE genSettFilePath(std::filesystem::path &);
RETCODE getCPUString(std::string &);
bool isSupportedCPU(const std::string &);
RETCODE loadSett(const std::unique_ptr<settings_t> &, const std::filesystem::path &);
uint32_t getWidgetValue(OPTID, const std::unique_ptr<settings_t> &);
int writeSMU(ryzen_access, uint32_t, OPTID);
void startInterval(std::function<void (cbData3 *)>, cbData3 *, const std::atomic<bool> &, uint);