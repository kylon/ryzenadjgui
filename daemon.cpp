#include <csignal>

#include "daemon.h"

extern std::vector<option_t> optsVec;

std::atomic<bool> running = false;

void signalHandler(int signal) {
    switch (signal) {
        case SIGINT:
        case SIGTERM:
        case SIGQUIT:
            running.store(false);
            break;
        default:
            break;
    }

    std::signal(signal, SIG_DFL);
}

void help(const std::string &binName) {
    std::cout << APP_NAME" CLI " APP_VERSION "\n\n"
              << "Usage:  " << binName << " [OPTION]\n\n"
              << "  Options:\n"
              << "    -d [interval]         Run as daemon\n"
              << "    -k                    Stop daemon\n"
              << "    -h                    Show this help\n\n"
              << "  [interval]: Optional argument.\n"
              << "     When > 0, override your saved setting and set a new interval for this session\n\n";
}

RETCODE parseArgs(int argc, char *argv[], uint &interval) {
    for (int i=1; i<argc; ++i) {
        if (strcasecmp("-d", argv[i]) == 0) {
            if (argc > 2) {
                std::string userV {argv[i+1]};

                if (std::all_of(userV.begin(), userV.end(), ::isdigit))
                    interval = std::atoi(userV.c_str());
                else
                    return RETCODE::ECLI_PARSE_ARGS;
            }

            return RETCODE::CLI_DAEMONIZE_ARG;

        } else if (strcasecmp("-k", argv[i]) == 0) {
            return RETCODE::CLI_KILLD_ARG;

        } else if (strcasecmp("-h", argv[i]) == 0) {
            return RETCODE::CLI_HELP_ARG;
        }
    }

    return RETCODE::ECLI_PARSE_ARGS;
}

RETCODE daemonize() {
    pid_t pid = 0;
    int ret = 0;

    pid = fork();
    if (pid < 0)
        return RETCODE::EDAEMONIZE_FORK;
    else if (pid > 0)
        exit(EXIT_SUCCESS);

    if (setsid() < 0)
        return RETCODE::EDAEMONIZE_SETSID;

    pid = fork();
    if (pid < 0)
        return RETCODE::EDAEMONIZE_FORK;
    else if (pid > 0)
        exit(EXIT_SUCCESS);

    std::signal(SIGCHLD, SIG_IGN);
    std::signal(SIGHUP, SIG_IGN);

    umask(0);

    ret = chdir("/");
    if (ret < 0)
        return RETCODE::EDAEMONIZE_CHDIR;

    std::cout << APP_NAME " daemon running with pid: " << getpid() << std::endl;

    for (int fd = sysconf(_SC_OPEN_MAX); fd>0; --fd)
		close(fd);

    stdin = fopen("/dev/null", "r");
	stdout = fopen("/dev/null", "w");
	stderr = fopen("/dev/null", "w+");

    std::signal(SIGINT, signalHandler);
    std::signal(SIGTERM, signalHandler);
    std::signal(SIGQUIT, signalHandler);

    return RETCODE::DAEMONIZE_OK;
}

void startDaemon(const std::unique_ptr<settings_t> &sett, uint interval, const std::filesystem::path &logPath) {
    std::ofstream out {logPath};
    int ryzErrorC = 0;

    running.store(true);
    std::cout.rdbuf(out.rdbuf());

    LOG("Daemon start [interval: "<<std::to_string(interval)<<"]")

    while (running.load() && ryzErrorC < 10) {
        auto slp = std::chrono::steady_clock::now() + std::chrono::seconds(interval);
        ryzen_access ry = init_ryzenadj();

        if (ry != nullptr) {
            ryzErrorC = 0; // reset

            for (auto const &o : optsVec) {
                uint32_t v = getWidgetValue(o.id, sett);

                if (writeSMU(ry, v, o.id) < 0);
                    std::cout << "error: value=" << std::to_string(v) << " id=" << std::to_string(o.id) << "\n";
            }

        } else {
            ++ryzErrorC;
        }

        cleanup_ryzenadj(ry);
        std::this_thread::sleep_until(slp);
    }

    if (ryzErrorC >= 10)
        std::cout << "RyzenAdj init error!\n";

    std::cout << "Daemon stop\n";
}

RETCODE stopDaemon(const std::string &appBin) {
    RETCODE ret = RETCODE::UNK_RETCODE;
    std::string pid;
    pid_t kpid;
    int res;

    ret = getRunningPid(appBin, pid, true);
    if (ret == RETCODE::EGETPID_CUR_PID)
        return RETCODE::KILL_DAEMON_OK;
    else if (ret != RETCODE::GETPID_OK)
        return RETCODE::EKILL_DAEMON;

    kpid = std::strtol(pid.c_str(), nullptr, 10);
    if (kpid == 0)
        return RETCODE::EKILL_DAEMON;

    res = kill(kpid, SIGINT);
    if (res != 0)
        return RETCODE::EKILL_DAEMON;

    return RETCODE::KILL_DAEMON_OK;
}