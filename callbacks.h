#pragma once

void dialogOk(Fl_Button *, void *);
void closeWindow(Fl_Widget *, void *);
void setOptionState(Fl_Check_Button *, void *);
void setOption(Fl_Hor_Value_Slider *, void *);
void resetSliders(Fl_Widget *, void *);
void applySett(Fl_Button *, void *);
void saveSett(Fl_Widget *, void *);
void setIntervalSett(Fl_Counter *, void *);
void applyThread(Fl_Check_Button *, void *);