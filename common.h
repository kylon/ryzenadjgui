#pragma once

#include <unistd.h>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include <thread>
#include <atomic>
#include <filesystem>
#include <cstdint>
#include <chrono>
#include <set>
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Hor_Value_Slider.H>
#include <FL/Fl_Counter.H>

#define APP_VERSION "1.0"
#define APP_NAME "RyzenAdjGUI"

#ifdef DEBUG
#include <mutex>

#define LOG(stream) {\
    extern std::mutex coutMux;\
    std::lock_guard<std::mutex> lock {coutMux};\
    std::cout << stream << "\n";\
    }
#else
#define LOG(stream)
#endif

enum RETCODE {
    UNK_RETCODE = 0,
    ECORRUPT_SETT_FILE,
    ENO_SETT_FILE,
    LOAD_SETT_OK,
    EUSERNAME_CALL_FAIL,
    USERNAME_CALL_OK,
    ENO_CPUINFO,
    EOPEN_CPUINFO,
    CPUINFO_OK,
    EINIT_RUNNING,
    EINIT_FAIL,
    EGETPID_FD,
    EGETPID_CLOSE_FD,
    EGETPID_TOO_MANY,
    EGETPID_CUR_PID,
    GETPID_OK,
    INIT_OK,
    EDAEMONIZE_FORK,
    EDAEMONIZE_SETSID,
    EDAEMONIZE_CHDIR,
    DAEMONIZE_OK,
    ECLI_PARSE_ARGS,
    CLI_HELP_ARG,
    CLI_DAEMONIZE_ARG,
    CLI_KILLD_ARG,
    EKILL_DAEMON,
    KILL_DAEMON_OK
};

enum OPTID {
    STAPMLIM = 0,
    FASTLIM,
    SLOWLIM,
    SLOWTIME,
    STAPMTIME,
    TCTLTEMP,
    VRMCURRENT,
    VRMSOCCURRENT,
    VRMMAXCURRENT,
    VRMSOCMAXCURRENT,
    PSI0CURRENT,
    PSI0SOCCURRENT,
    MAXSOCCLOCK,
    MINSOCCLOCK,
    MAXFCLOCK,
    MINFCLOKC,
    MAXVCN,
    MINVCN,
    MAXLCLOCK,
    MINLCLOCK,
    MAXGFXCLOCK,
    MINGFXCLOCK,
    NOOPT
};

struct option_t {
    std::string label;
    std::string desc;
    uint32_t minVal;
    uint32_t maxVal;
    uint32_t defVal;
    int step;
    OPTID id; // callback id
    bool enabled;
    bool active;
};

struct settings_t {
    uint32_t stapmLim;
    uint32_t fastLim;
    uint32_t slowLim;
    uint32_t slowTime;
    uint32_t stapmTime;
    uint32_t tctlTemp;
    uint32_t vrmCurrent;
    uint32_t vrmSocCurrent;
    uint32_t vrmMaxCurrent;
    uint32_t vrmSocMaxCurrent;
    uint32_t psi0Current;
    uint32_t psi0SocCurrent;
    uint32_t maxSocClkFrequency;
    uint32_t minSocClkFrequency;
    uint32_t maxFclkFrequency;
    uint32_t minFclkFrequency;
    uint32_t maxVcn;
    uint32_t minVcn;
    uint32_t maxLclk;
    uint32_t minLclk;
    uint32_t maxGfxClk;
    uint32_t minGfxClk;
    uint reapplySecs;
};

struct cbData {
    std::set<OPTID> &changedLst;
    settings_t *sett;
    int widgetIdx; // widget to manipulate
    int oIdx; // option vector idx

    cbData(int wid, int idx, settings_t *s, std::set<OPTID> &cl): widgetIdx(wid), oIdx(idx), sett(s), changedLst(cl) {}
};

struct cbData2 {
    std::filesystem::path settPath;
    settings_t *sett;

    cbData2(settings_t *s, std::filesystem::path p): sett(s), settPath(p) {}
};

struct cbData3 {
    std::vector<std::unique_ptr<Fl_Hor_Value_Slider>> &sliders;
    std::set<OPTID> &changedLst;
    settings_t *sett;
    int widgetIdx; // widget to manipulate
    int oIdx; // option vector idx

    cbData3(int wid, int idx, settings_t *s, std::vector<std::unique_ptr<Fl_Hor_Value_Slider>> &v, std::set<OPTID> &cl):
            widgetIdx(wid), oIdx(idx), sett(s), sliders(v), changedLst(cl) {}
};

struct cbData4 {
    cbData3 *cbdata;
    std::atomic<bool> &thrdRunFlag;

    cbData4(cbData3 *c, std::atomic<bool> &f): cbdata(c), thrdRunFlag(f) {}
};