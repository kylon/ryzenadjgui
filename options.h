#pragma once

#include "common.h"

std::vector<option_t> optsVec {
    option_t {"Stapm Limit", "Sustained power limit (W)", 7, 65, 15, 1, OPTID::STAPMLIM, true, false},
    option_t {"Fast Limit", "Fast PPT power limit (W)", 7, 65, 15, 1, OPTID::FASTLIM, true, false},
    option_t {"Slow Limit", "Slow PPT power limit (W)", 7, 65, 10, 1, OPTID::SLOWLIM, true, false},
    option_t {"Slow Time", "Slow PPT constant time (S)", 1, 3600, 900, 1, OPTID::SLOWTIME, true, false},
    option_t {"Stapm Time", "STAPM constant time (S)", 1, 3600, 900, 1, OPTID::STAPMTIME, true, false},
    option_t {"Tctl Temp", "Tctl temperature (°C)", 50, 100, 80, 1, OPTID::TCTLTEMP, true, false},
    option_t {"Max GFX Clock", "Maximum GFX Clock (Mhz)", 300, 1300, 1100, 10, OPTID::MAXGFXCLOCK, true, false},
    option_t {"Min GFX Clock", "Minimum GFX Clock (Mhz)", 300, 1300, 300, 10, OPTID::MINGFXCLOCK, true, false},
    option_t {"VRM Current", "Voltage Regulator Module Current Limit (A)", 20, 100, 20, 1, OPTID::VRMCURRENT, true, false},
    option_t {"VRM Max Current", "Voltage Regulator Module Maximum Current Limit (A)", 20, 100, 30, 1, OPTID::VRMMAXCURRENT, true, false},
    option_t {"VRM SoC Current", "Voltage Regulator Module SoC Current Limit (A)", 0, 0, 0, 1, OPTID::VRMSOCCURRENT, false, false},
    option_t {"VRM SoC Max Current", "Voltage Regulator Module SoC Maximum Current Limit (A)", 0, 0, 0, 1, OPTID::VRMSOCMAXCURRENT, false, false},
    option_t {"PSI0 Current", "Power State Indicator 0 Current Limit (mA)", 20, 100, 20, 1, OPTID::PSI0CURRENT, true, false},
    option_t {"PSI0 SoC Current", "Power State Indicator 0 SoC Current Limit (mA)", 0, 0, 0, 1, OPTID::PSI0SOCCURRENT, false, false},
    option_t {"Max SoC Clock", "Maximum SoC Clock Frequency (MHz)", 200, 750, 600, 10, OPTID::MAXSOCCLOCK, true, false},
    option_t {"Min SoC Clock", "Minimum SoC Clock Frequency (MHz)", 200, 750, 200, 10, OPTID::MINSOCCLOCK, true, false},
    option_t {"Max fclock", "Maximum Transmission (CPU-GPU) Frequency (MHz)", 800, 1600, 1200, 10, OPTID::MAXFCLOCK, true, false},
    option_t {"Min fclock", "Minimum Transmission (CPU-GPU) Frequency (MHz)", 800, 1600, 800, 10, OPTID::MINFCLOKC, true, false},
    option_t {"Max VCN", "Maximum Video Core Next (VCE - Video Coding Engine) (Value)", 0, 0, 0, 1, OPTID::MAXVCN, false, false},
    option_t {"Min VCN", "Minimum Video Core Next (VCE - Video Coding Engine) (Value)", 0, 0, 0, 1, OPTID::MINVCN, false, false},
    option_t {"Max lclock", "Maximum Data Launch Clock (Value)", 0, 0, 0, 1, OPTID::MAXLCLOCK, false, false},
    option_t {"Min lclock", "Minimum Data Launch Clock (Value)", 0, 0, 0, 1, OPTID::MINLCLOCK,false, false}
};
