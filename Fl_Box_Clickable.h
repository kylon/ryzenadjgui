#pragma once

#include "common.h"

class Fl_Box_Clickable : Fl_Box {
    int highlight = 0;

public:
    Fl_Box_Clickable(int x, int y, int w, int h, const char *label = 0) : Fl_Box(x, y, w, h, label) {}

    void box(Fl_Boxtype t) {
        Fl_Box::box(t);
    }

    void callback(Fl_Callback *w, void *user_data) {
        Fl_Box::callback(w, user_data);
    }

    void color(Fl_Color c) {
        Fl_Box::color(c);
    }

    int handle(int event) {
        switch(event) {
            case FL_PUSH:
                this->highlight = 1;
                redraw();
                return 1;
            case FL_RELEASE:
                if (this->highlight) {
                    this->highlight = 0;
                    redraw();
                    do_callback();
                    // never do anything after a callback, as the callback
                    // may delete the widget!
                }
                return 1;
            default:
                return Fl_Widget::handle(event);
        }
    }
};