#include "utils.h"

std::vector<std::string> splitString(const std::string &str, const std::string &delim) {
    std::vector<std::string> ret;
    std::string t = str;
    std::string token;
    std::size_t pos = 0;

    while ((pos = t.find(delim)) != std::string::npos) {
        token = t.substr(0, pos);

        t.erase(0, pos + delim.length());
        ret.push_back(token);
    }

    ret.push_back(t);

    return ret;
}

bool isRoot() {
    return getuid() == 0;
}

RETCODE getRunningPid(const std::string &appName, std::string &pid, bool isKiller) {
    std::string cmd {"pidof " + appName};
    std::vector<std::string> pids;
    char buf[100] = {0};
    int ret = 0;
    FILE *fd;

    fd = popen(cmd.c_str(), "r");
    if (fd == nullptr)
        return RETCODE::EGETPID_FD;

    std::fgets(buf, sizeof(buf), fd);

    ret = pclose(fd);
    if (ret < 0)
        return RETCODE::EGETPID_CLOSE_FD;

    LOG("getRunningPid: "<<buf)

    pids = splitString(std::string {buf}, " ");

    if ((pids.size() > 1 && !isKiller) || (pids.size() > 2))
        return RETCODE::EGETPID_TOO_MANY;
    else if (isKiller && pids.size() == 1)
        return RETCODE::EGETPID_CUR_PID;

    pid = isKiller ? pids[1]:pids[0];

    return RETCODE::GETPID_OK;
}

bool isAlreadyRunning(const std::string &appName) {
    std::string pid;
    RETCODE ret;

    ret = getRunningPid(appName, pid, false);

    return ret != RETCODE::GETPID_OK;
}

RETCODE genSettFilePath(std::filesystem::path &path) {
  char username[256] = {0};

  getlogin_r(username, sizeof(username));
  if (username == nullptr)
    return RETCODE::EUSERNAME_CALL_FAIL;

  path.assign("/home/" + std::string{username} + "/.ryzenadjgui/settings.bin");
  path = path.make_preferred();

  return RETCODE::USERNAME_CALL_OK;
}

RETCODE getCPUString(std::string &str) {
  std::filesystem::path cpuinfo {"/proc/cpuinfo"};
  std::ifstream fcpu {cpuinfo};
  std::string line;

  if (!std::filesystem::exists(cpuinfo))
    return RETCODE::ENO_CPUINFO;
  else if (!fcpu.is_open())
    return RETCODE::EOPEN_CPUINFO;

  while (getline(fcpu, line)) {
    if (line.find("model name") == std::string::npos)
        continue;

    std::size_t atPos = line.find('@');

    if (atPos != std::string::npos)
        line.replace(atPos, 1, "-");

    str.assign(line.substr(line.find(':')+2));
    break;
  }

  return RETCODE::CPUINFO_OK;
}

bool isSupportedCPU(const std::string &cpuStr) {
    return cpuStr.find("Ryzen") != std::string::npos;
}

RETCODE loadSett(const std::unique_ptr<settings_t> &sett, const std::filesystem::path &path) {
    if (!std::filesystem::exists(path))
        return RETCODE::ENO_SETT_FILE;

  std::ifstream in {path, std::ios::binary};

  if (!in.is_open())
    return RETCODE::ECORRUPT_SETT_FILE;

  in.read(reinterpret_cast<char *>(sett.get()), sizeof(settings_t));

  return RETCODE::LOAD_SETT_OK;
}

uint32_t getWidgetValue(OPTID id, const std::unique_ptr<settings_t> &sett) {
    switch (id) {
        case OPTID::STAPMLIM:
            return sett->stapmLim;
        case OPTID::FASTLIM:
            return sett->fastLim;
        case OPTID::SLOWLIM:
            return sett->slowLim;
        case OPTID::SLOWTIME:
            return sett->slowTime;
        case OPTID::STAPMTIME:
            return sett->stapmTime;
        case OPTID::TCTLTEMP:
            return sett->tctlTemp;
        case OPTID::VRMCURRENT:
            return sett->vrmCurrent;
        case OPTID::VRMSOCCURRENT:
            return sett->vrmSocCurrent;
        case OPTID::VRMMAXCURRENT:
            return sett->vrmMaxCurrent;
        case OPTID::MAXGFXCLOCK:
            return sett->maxGfxClk;
        case OPTID::MINGFXCLOCK:
            return sett->minGfxClk;
        case OPTID::PSI0CURRENT:
            return sett->psi0Current;
        case OPTID::PSI0SOCCURRENT:
            return sett->psi0SocCurrent;
        case OPTID::MAXSOCCLOCK:
            return sett->maxSocClkFrequency;
        case OPTID::MINSOCCLOCK:
            return sett->minSocClkFrequency;
        case OPTID::MAXFCLOCK:
            return sett->maxFclkFrequency;
        case OPTID::MINFCLOKC:
            return sett->minFclkFrequency;
        case OPTID::MAXVCN:
            return sett->maxVcn;
        case OPTID::MINVCN:
            return sett->minVcn;
        case OPTID::MAXLCLOCK:
            return sett->maxLclk;
        case OPTID::MINLCLOCK:
            return sett->minLclk;
        default:
            return 0;
    }
}

int writeSMU(ryzen_access ry, uint32_t v, OPTID id) {
#ifdef DBG_UNSUP
    return 0; // success on unsupported cpus
#endif

    switch (id) {
        case OPTID::STAPMLIM:
            return set_stapm_limit(ry, v*1000);
        case OPTID::FASTLIM:
            return set_fast_limit(ry, v*1000);
        case OPTID::SLOWLIM:
            return set_slow_limit(ry, v*1000);
        case OPTID::SLOWTIME:
            return set_slow_time(ry, v*1000);
        case OPTID::STAPMTIME:
            return set_stapm_time(ry, v*1000);
        case OPTID::TCTLTEMP:
            return set_tctl_temp(ry, v);
        case OPTID::VRMCURRENT:
            return set_vrm_current(ry, v*1000);
        case OPTID::VRMMAXCURRENT:
            return set_vrmmax_current(ry, v*1000);
        case OPTID::PSI0CURRENT:
            return set_psi0_current(ry, v);
        case OPTID::MAXSOCCLOCK:
            return set_max_socclk_freq(ry, v);
        case OPTID::MINSOCCLOCK:
            return set_min_socclk_freq(ry, v);
        case OPTID::MAXFCLOCK:
            return set_max_fclk_freq(ry, v);
        case OPTID::MINFCLOKC:
            return set_min_fclk_freq(ry, v);
        case OPTID::MAXGFXCLOCK:
            return set_max_gfxclk_freq(ry, v);
        case OPTID::MINGFXCLOCK:
            return set_min_gfxclk_freq(ry, v);
        default:
            return -1;
    }
}

void startInterval(std::function<void (cbData3 *)> func, cbData3 *data, const std::atomic<bool> &thrdFlag, uint interval) {
    std::thread {
        [func, data, &thrdFlag, interval]() {
            while (thrdFlag.load()) {
                auto slp = std::chrono::steady_clock::now() + std::chrono::seconds(interval);

                LOG("run applyThread..")
                func(data);

                std::this_thread::sleep_until(slp);
            }
        }
    }.detach();
}