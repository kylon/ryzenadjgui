# RyzenAdjGUI

![ryzenadjgui logo](img/ryadjgui_logo.png)

A simple and lightweight GUI for RyzenAdj

Author: kylon  
Version: 1.0  
License: GPLv3  
  

## Supported OSes
* Linux


## Requirements
* FLTK
* pciutils or libpci-dev (See your Distro)


## How to run as daemon
You can run RyzenAdjGUI as daemon with the following command:

```./ryzenadjGUI -d```


An optional parameter is available, the interval in seconds, ie, how often it should apply your settings.


This will override your `Reapply Interval` setting.

```./ryzenadjGUI -d 10```


## CLI options
```
	-d [interval]  Run as daemon
	-k             Stop Daemon
	-h             Show Help
```


## How to build
```
	git clone --recursive  
	cd RyzenAdjGUI  
	mkdir build  
	cd build  
	cmake ..  
	make  
```


## External Sources
* [ryzenadj](https://github.com/FlyGoat/RyzenAdj)
* [Ryzen Controller](https://gitlab.com/ryzen-controller-team/ryzen-controller)


## Notes
RyzenAdj cannot read your current settings yet, so `default` is just a reasonable value.  
To check your settings use [AMD uProf](https://developer.amd.com/amd-uprof/)


## Credits
* FlyGoat - ryzenadj
* Ryzen controller Team - Ryzen controller
