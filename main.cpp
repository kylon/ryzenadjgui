
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_PNG_Image.H>

#include "Fl_Box_Clickable.h"
#include "callbacks.h"
#include "options.h"
#include "utils.h"
#include "daemon.h"

#ifdef DEBUG
std::mutex coutMux;
#endif
bool asDaemon = false;

int dialog(const std::string &title, const std::string &msg) {
  if (asDaemon) {
    std::cout << msg << "\n";
    return 0;
  }

  std::unique_ptr<Fl_Window> dialog = std::make_unique<Fl_Window>(300, 100, title.c_str());
  std::unique_ptr<Fl_Button> button = std::make_unique<Fl_Button>(0, 0, 0, 0, "Ok");
  std::unique_ptr<Fl_Box> text = std::make_unique<Fl_Box>(0, 0, 0, 0);
  int labelW = 0, labelH = 0;

  text->label(msg.c_str());
  text->measure_label(labelW, labelH);

  dialog->resize(dialog->x(), dialog->y(), labelW+100, labelH+60);
  dialog->redraw();

  button->color(FL_WHITE);
  button->selection_color(FL_WHITE);
  button->box(FL_FLAT_BOX);
  button->labelcolor(FL_BLACK);
  button->resize(0, dialog->h()-30, dialog->w(), 30);
  button->redraw();

  text->resize(0, 0, dialog->w(), dialog->h()-30);
  text->redraw();

  button->callback(reinterpret_cast<Fl_Callback *>(dialogOk), dialog.get());
  button->set_visible_focus();

  dialog->set_modal();
  dialog->end();
  dialog->show();

  return Fl::run();
}

int about() {
  extern const unsigned char _binary_img_ryadjgui_logo_png_start;
  extern const unsigned char _binary_img_ryadjgui_logo_png_end;

  int imgSz = sizeof(char) * (&_binary_img_ryadjgui_logo_png_end - &_binary_img_ryadjgui_logo_png_start);
  std::unique_ptr<Fl_Double_Window> aboutW = std::make_unique<Fl_Double_Window>(400, 420, "About");
  std::unique_ptr<Fl_Image> ryzenLogo = std::make_unique<Fl_PNG_Image>(nullptr, &_binary_img_ryadjgui_logo_png_start, imgSz);
  std::unique_ptr<Fl_Box> imgBox = std::make_unique<Fl_Box>(0, 0, 400, 225);
  std::unique_ptr<Fl_Box_Clickable> boxWk = std::make_unique<Fl_Box_Clickable>(0, 0, aboutW->w(), aboutW->h());
  int ret = ryzenLogo->fail();
  std::unique_ptr<Fl_Box> text = std::make_unique<Fl_Box>((aboutW->w()/2)-70, imgBox->h()+60, 210, 80);

  text->box(FL_NO_BOX);
  text->labelcolor(fl_rgb_color(165, 10, 17));
  text->labelfont(FL_BOLD);
  text->align(FL_ALIGN_INSIDE | FL_ALIGN_LEFT | FL_ALIGN_NOWRAP);
  text->label("Version:    " APP_VERSION "\n"
    "Author:     Kylon\n"
    "License:    GPLv3\n"
    "Year:         2019\n\n\n"
    "Thanks to:\n\n"
    "FlyGoat\n"
    "Ryzen Controller Team"
  );

  boxWk->box(FL_NO_BOX);
  boxWk->callback(reinterpret_cast<Fl_Callback *>(closeWindow), aboutW.get());

  aboutW->border(false);
  aboutW->color(fl_rgb_color(255, 120, 0));
  aboutW->set_modal();
  aboutW->end();
  aboutW->show();

  if (ret < 0)
    dialog("Error", "Cannot load logo image! Code: " + std::to_string(ret));
  else
    imgBox->image(*ryzenLogo);

  return Fl::run();
}

int options(Fl_Widget *, void *data) {
  std::unique_ptr<Fl_Double_Window> optW = std::make_unique<Fl_Double_Window>(250, 320, "Options");
  std::unique_ptr<Fl_Counter> secsInpt = std::make_unique<Fl_Counter>(25, 30, 200, 30, "Reapply Interval (Sec)");
  std::unique_ptr<Fl_Button> okBtn = std::make_unique<Fl_Button>(optW->w()-70, optW->h()-40, 60, 30, "Save");
  std::unique_ptr<Fl_Button> cancBtn = std::make_unique<Fl_Button>(okBtn->x()-okBtn->w()-5, okBtn->y(), okBtn->w(), okBtn->h(), "Close");
  std::unique_ptr<Fl_Box> secsInptHelp = std::make_unique<Fl_Box>(secsInpt->x()-15, secsInpt->y()+secsInpt->h()+15, secsInpt->w()+30, 30);
  cbData2 *cdata = static_cast<cbData2 *>(data);

  secsInpt->align(FL_ALIGN_TOP | FL_ALIGN_CENTER);
  secsInpt->labelfont(FL_BOLD);
  secsInpt->labelcolor(fl_rgb_color(190, 0, 0));
  secsInpt->step(1.0);
  secsInpt->lstep(100.0);
  secsInpt->minimum(1.0);
  secsInpt->value(cdata->sett->reapplySecs ? cdata->sett->reapplySecs:secsInpt->minimum());
  secsInpt->callback(reinterpret_cast<Fl_Callback *>(setIntervalSett), cdata);

  secsInptHelp->label("Req. service restart to take effect.\n[Disable and enable reapply automatically]");
  secsInptHelp->labelfont(FL_ITALIC);
  secsInptHelp->labelsize(13);
  secsInptHelp->labelcolor(FL_DARK_BLUE);
  secsInptHelp->align(FL_ALIGN_WRAP);

  okBtn->box(FL_FLAT_BOX);
  okBtn->color(fl_rgb_color(190, 0, 0));
  okBtn->labelcolor(FL_WHITE);
  okBtn->selection_color(FL_DARK_RED);
  okBtn->clear_visible_focus();
  okBtn->callback(reinterpret_cast<Fl_Callback *>(saveSett), cdata);

  cancBtn->box(FL_FLAT_BOX);
  cancBtn->color(fl_rgb_color(190, 0, 0));
  cancBtn->labelcolor(FL_WHITE);
  cancBtn->selection_color(FL_DARK_RED);
  cancBtn->clear_visible_focus();
  cancBtn->callback(reinterpret_cast<Fl_Callback *>(closeWindow), optW.get());

  optW->set_modal();
  optW->color(fl_rgb_color(246, 150, 0));
  optW->resizable(optW.get());
  optW->end();
  optW->show();

  return Fl::run();
}

void createOptsSliders(std::vector<std::unique_ptr<Fl_Hor_Value_Slider>> &vec, int applyBtnIdx,
                        const std::unique_ptr<settings_t> &sett, std::set<OPTID> &changedLst,
                        std::vector<std::unique_ptr<Fl_Check_Button>> &chkVec) {
  int x = 50;
  int y = 80;
  int w = 225;
  int h = 30;
  int i = 0;
  int j = 0;
  int z = 0;

  for (auto const &o : optsVec) {
      if (!o.enabled) {
        ++z;
        continue;
      }

    std::unique_ptr<Fl_Hor_Value_Slider> slider = std::make_unique<Fl_Hor_Value_Slider>(x, y, w, h, o.label.c_str());
    std::unique_ptr<Fl_Check_Button> checkbox = std::make_unique<Fl_Check_Button>(x-25, y+7, 15, 15);
    uint32_t value = getWidgetValue(o.id, sett);
    cbData *datacb = new cbData {applyBtnIdx, z, sett.get(), changedLst};
    cbData *datacb2 = new cbData {applyBtnIdx, z, nullptr, changedLst};

    slider->align(FL_ALIGN_TOP);
    slider->tooltip(o.desc.c_str());
    slider->slider_size(0.1);
    slider->box(FL_THIN_UP_BOX);
    slider->labelfont(FL_BOLD);
    slider->labelcolor(fl_rgb_color(190, 0, 0));
    slider->selection_color(fl_rgb_color(219, 26, 0));
    slider->color(fl_rgb_color(247, 247, 241));
    slider->value(value == 0 ? o.defVal:value);
    slider->bounds(o.minVal, o.maxVal);
    slider->step(o.step);
    slider->callback(reinterpret_cast<Fl_Callback *>(setOption), datacb);
    slider->redraw();
    vec.push_back(std::move(slider));

    checkbox->box(FL_NO_BOX);
    checkbox->clear_visible_focus();
    checkbox->selection_color(FL_DARK_GREEN);
    checkbox->down_box(FL_FLAT_BOX);
    checkbox->callback(reinterpret_cast<Fl_Callback *>(setOptionState), datacb2);
    chkVec.push_back(std::move(checkbox));

    i = i+1 < 8 ? ++i:0;
    x = (w*j) + (55*j+50);
    y = (h*i) + (32*i+80);
    j = i == 7 ? ++j:j;
    ++z;
  }
}

RETCODE init(std::string &cpuStr, std::filesystem::path &path, const std::unique_ptr<settings_t> &sett, const std::string &appBin) {
  RETCODE ret = RETCODE::UNK_RETCODE;

  if (isAlreadyRunning(appBin))
    return RETCODE::EINIT_RUNNING;

  if (!asDaemon) {
    Fl::visual(FL_DOUBLE|FL_INDEX);
    Fl::background(240,240,240); // def background for all windows
  }

  ret = getCPUString(cpuStr);
  if (ret != RETCODE::CPUINFO_OK) {
    dialog("Error", "Could not open cpuinfo!\nError code: " + std::to_string(ret));
    return RETCODE::EINIT_FAIL;
  }

  if (!isSupportedCPU(cpuStr)) {
    dialog("Error", "Unsupported CPU detected!\nThis is only available for Ryzen CPUs.");
#ifndef DBG_UNSUP
    return RETCODE::EINIT_FAIL;
#endif
  }

  if (!isRoot()) {
    dialog("Error", "Due to its nature, " APP_NAME " must be run as root.");
    return RETCODE::EINIT_FAIL;
  }

  ret = genSettFilePath(path);
  if (ret == RETCODE::USERNAME_CALL_OK) {
    ret = loadSett(sett, path);

    if (ret == RETCODE::ECORRUPT_SETT_FILE)
      dialog("Error", "Failed to load settings from disk!");

  } else {
      dialog("Error", "Failed to get your Username!\nYou will not be able to save/load your settings.");
  }

  return RETCODE::INIT_OK;
}

int ryzenAdjGui(const std::string &cpuStr, const std::unique_ptr<settings_t> &sett, const std::filesystem::path &path) {
  std::unique_ptr<Fl_Double_Window> window = std::make_unique<Fl_Double_Window>(600, 620, "RyzenAdj GUI");
  std::unique_ptr<Fl_Menu_Bar> menuBar = std::make_unique<Fl_Menu_Bar>(0, 0, window->w(), 30);
  std::unique_ptr<Fl_Box> cpuString = std::make_unique<Fl_Box>(0, window->h()-20, window->w(), 20);
  std::unique_ptr<Fl_Box> creds = std::make_unique<Fl_Box>(window->w()-145, window->h()-20, 145, 20);
  std::unique_ptr<Fl_Button> applyBtn = std::make_unique<Fl_Button>(window->w()-50, 0, 50, 30, "Apply");
  std::unique_ptr<Fl_Check_Button> timerBtn = std::make_unique<Fl_Check_Button>(window->x()+5, window->h()-50, 200, 30, " Reapply automatically");
  std::vector<std::unique_ptr<Fl_Hor_Value_Slider>> sliders;
  std::vector<std::unique_ptr<Fl_Check_Button>> slidersChk;
  int applyBtnIdx = window->find(applyBtn.get());
  std::atomic<bool> applyThrdRun = false;
  std::set<OPTID> changedLst;
  std::unique_ptr<cbData2> saveCData = std::make_unique<cbData2>(sett.get(), path);
  std::unique_ptr<cbData3> applyCData = std::make_unique<cbData3>(-1, -1, nullptr, sliders, changedLst);
  std::unique_ptr<cbData3> resetCData = std::make_unique<cbData3>(applyBtnIdx, -1, sett.get(), sliders, changedLst);
  std::unique_ptr<cbData3> applyThrdCData3 = std::make_unique<cbData3>(applyBtnIdx, -1, sett.get(), sliders, changedLst);
  std::unique_ptr<cbData4> applyThrdCData = std::make_unique<cbData4>(applyThrdCData3.get(), applyThrdRun);
  Fl_Menu_Item menuitems[] = {
    {"Save", 0, reinterpret_cast<Fl_Callback *>(saveSett), saveCData.get(), 0, 0, 0, 0, FL_WHITE},
    {"Reset Defaults", 0, reinterpret_cast<Fl_Callback *>(resetSliders), resetCData.get(), 0, 0, 0, 0, FL_WHITE},
    {"Options", 0, reinterpret_cast<Fl_Callback *>(options), saveCData.get(), 0, 0, 0, 0, FL_WHITE},
    {"About", 0, reinterpret_cast<Fl_Callback *>(about), 0, 0, 0, 0, 0, FL_WHITE},
    {0}
  };

  applyBtn->box(FL_FLAT_BOX);
  applyBtn->color(FL_BLACK);
  applyBtn->labelcolor(FL_WHITE);
  applyBtn->clear_visible_focus();
  applyBtn->selection_color(FL_WHITE);
  applyBtn->callback(reinterpret_cast<Fl_Callback *>(applySett), applyCData.get());

  menuBar->menu(menuitems);
  menuBar->box(FL_FLAT_BOX);
  menuBar->color(FL_BLACK);
  menuBar->selection_color(FL_WHITE);

  cpuString->box(FL_FLAT_BOX);
  cpuString->align(FL_ALIGN_INSIDE | FL_ALIGN_BOTTOM_LEFT);
  cpuString->labelcolor(FL_WHITE);
  cpuString->labelfont(FL_BOLD);
  cpuString->label(cpuStr.c_str());
  cpuString->color(fl_rgb_color(220, 120, 0));

  creds->box(FL_NO_BOX);
  creds->align(FL_ALIGN_INSIDE | FL_ALIGN_RIGHT);
  creds->labelcolor(FL_WHITE);
  creds->label("Kylon - 2019");

  createOptsSliders(sliders, applyBtnIdx, sett, changedLst, slidersChk);

  timerBtn->box(FL_NO_BOX);
  timerBtn->clear_visible_focus();
  timerBtn->selection_color(FL_DARK_GREEN);
  timerBtn->down_box(FL_FLAT_BOX);
  timerBtn->labelfont(FL_BOLD);
  timerBtn->labelcolor(fl_rgb_color(190, 0, 0));
  timerBtn->callback(reinterpret_cast<Fl_Callback *>(applyThread), applyThrdCData.get());

  window->color(fl_rgb_color(246, 155, 0));
  window->resizable(window.get());
  window->end();
  window->show();

  return Fl::run();
}

int main(int argc, char *argv[]) {
  std::unique_ptr<settings_t> sett = std::make_unique<settings_t>();
  RETCODE ret = RETCODE::UNK_RETCODE;
  std::filesystem::path appBin {argv[0]};
  std::filesystem::path settFPath;
  std::string cpuStr;
  uint dInterval = 0;

  if (argc > 1)
    asDaemon = true;

  appBin = appBin.filename();

  ret = init(cpuStr, settFPath, sett, appBin);
  if (ret == RETCODE::EINIT_FAIL)
    return 1;

  if (asDaemon) { // CLI
    RETCODE dret = RETCODE::UNK_RETCODE;

    dret = parseArgs(argc, argv, dInterval);

    switch (dret) {
      case RETCODE::CLI_DAEMONIZE_ARG: {
        if (ret == RETCODE::EINIT_RUNNING) {
          std::cout << "Error: " APP_NAME " is already running!\n";
          return 1;
        }

        std::filesystem::path logPath = settFPath;

        dret = daemonize();
        if (dret != RETCODE::DAEMONIZE_OK) {
          std::cout << APP_NAME " fatal error: " << std::to_string(dret) << "\n";
          return 1;
        }

        logPath.remove_filename();
        logPath += "daemon-log.txt";

        startDaemon(sett, dInterval > 0 ? dInterval:sett->reapplySecs, logPath);
      }
        break;
      case RETCODE::CLI_KILLD_ARG: {
        dret = stopDaemon(appBin);

        if (dret != RETCODE::KILL_DAEMON_OK)
          std::cout << APP_NAME ": stop daemon error: " << std::to_string(dret) << "\n";
      }
        break;
      case RETCODE::CLI_HELP_ARG:
      case RETCODE::ECLI_PARSE_ARGS:
      default:
        help(appBin);
        break;
    }

  } else { // GUI
    if (ret == EINIT_RUNNING)
      dialog("Error", APP_NAME " is already running!");
    else if (ret == INIT_OK)
      ryzenAdjGui(cpuStr, sett, settFPath);
  }

  return 0;
}